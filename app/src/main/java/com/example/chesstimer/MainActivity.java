package com.example.chesstimer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import com.example.chesstimer.databinding.ActivityMainBinding;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private static final long START_TIME_IN_MILLIS = 600000;
    private CountDownTimer PlayerOneCountDownTimer, PlayerTwoCountDownTimer;
    private boolean playerOneRunning, playerTwoRunning;
    private long playerOneTimeLeftInMillis = START_TIME_IN_MILLIS;
    private long playerTwoTimeLeftInMillis = START_TIME_IN_MILLIS;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding.playerOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (playerOneRunning) {
                    pausePlayerOneTimer();
                    startPlayerTwoTimer();

                } else {
                    startPlayerOneTimer();
                }
            }
        });

        binding.playerTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (playerTwoRunning) {
                pausePlayerTwoTimer();
                startPlayerOneTimer();
            } else {
                startPlayerTwoTimer();
            } }
        });

    }

    private void pausePlayerOneTimer() {
        PlayerOneCountDownTimer.cancel();
        playerOneRunning = false;

    }

    private void pausePlayerTwoTimer() {
        PlayerTwoCountDownTimer.cancel();
        playerTwoRunning = false;
    }



    private void startPlayerOneTimer() {
        PlayerOneCountDownTimer = new CountDownTimer(playerOneTimeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                playerOneTimeLeftInMillis = millisUntilFinished;
                updatePlayerOneTimer();
            }

            @Override
            public void onFinish() {
                playerOneRunning = false;
            }
        }.start();
        playerOneRunning = true;

    }

    private void startPlayerTwoTimer() {
        PlayerTwoCountDownTimer = new CountDownTimer(playerTwoTimeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                playerTwoTimeLeftInMillis = millisUntilFinished;
                updatePlayerTwoTimer();
            }

            @Override
            public void onFinish() {
                playerTwoRunning = false;
            }
        }.start();
        playerTwoRunning = true;

    }

    private void updatePlayerOneTimer() {
        int minutes = (int) (playerOneTimeLeftInMillis / 1000) / 60;
        int seconds = (int) (playerOneTimeLeftInMillis / 1000) % 60;

        String playerOneTimeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        binding.pOneTimer.setText(playerOneTimeLeftFormatted);
    }

    private void updatePlayerTwoTimer() {
        int minutes = (int) (playerTwoTimeLeftInMillis / 1000) / 60;
        int seconds = (int) (playerTwoTimeLeftInMillis / 1000) % 60;

        String playerTwoTimeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        binding.pOneTimer.setText(playerTwoTimeLeftFormatted);
    }


}